package com.inyek.ubisport.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by TEL-C on 9/2/17.
 */

public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this.getApplicationContext();
    }

    public static Context getmContext() {
        return mContext;
    }
}
